pipeline {

    // Which jenkins server will be running the pipeline
    agent any

    parameters {

        // Test Names

        string(name: 'apiTests', defaultValue: 'ApiTestsIT', description: 'API tests')
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')

        // Website parameters

        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://18.236.63.138', description: 'domain of the test environment')

        // Test Environment Parameter
        string(name: 'test_hostname', defaultValue: '18.236.63.138', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test env')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of tomcat')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of tomcat server')

    }

    options {
        // this allows us to only keep the artifacts and build logs of the last 3 builds
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3' ))
    }

        // poll every minute
    triggers {
        pollSCM('* * * * *')

        // poll every 5 minutes and avoid conflicts
        // pollSCM('H/5 * * * *')
    }

    stages {

        stage('Build with Unit Testing') {

            steps {

                // adding sonarqube after the code is packaged
                sh 'mvn clean package sonar:sonar -Dsonar.organization=safebear-student-bitbucket -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=f08a36f3598527bf9b20d9b861b57c0aaa5937e3'

            }

            post {

                success {

                    echo 'Now archiving...'

                    archiveArtifacts artifacts: '**/target/*.war'
                }

                always {

                    junit "**/target/surefire-reports/*.xml"
                }

            }

        }

        stage('Static Analysis') {

            steps {

                sh 'mvn checkstyle:checkstyle'

            }

            post {

                success {

                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''

                }

            }

        }

        stage('Deploy to Test') {

            steps {
                sh "mvn cargo:redeploy -Dcargo.hostname=${test_hostname} -Dcargo.username=${test_username} -Dcargo.password=${test_password} -Dcargo.servlet.port=${test_port}"


            }

        }

        stage('Run API Tests') {

            steps {

                // sh 'mvn -Dtest=${apiTests} test -Ddomain=${domain} -Dport=${test_port} -Dcontext=${context}'
                sh 'newman run src/test/collections/tasklist.postman_collection.json -e src/test/collections/testenv.postman_environment.json --bail newman'
            }

//            post {
//                always {
//                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
//                }
//            }


        }


        stage('cucumber bdd tests') {

            steps {

                // Make sure the chromedriver is at the latest level
                sh 'mvn clean -Dtest=${cuke} test -Ddomain=${domain} -Dport=${test_port} -Dcontext=${context} -Dsleep="0" -Dbrowser="headless"'

            }
            post {
                always {
                    publishHTML([
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : false,
                            reportDir            : 'target/cucumber',
                            reportFiles          : 'extent_report.html',
                            reportName           : 'BDD report',
                            reportTitles         : ''
                    ])
                }
            }

        }

    }


}